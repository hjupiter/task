from connexion.apps.flask_app import FlaskJSONEncoder
import six

from datetime import date


def date_encoder(obj):
    if isinstance(obj, date):
        return obj.strftime("%d/%m/%Y")