from swagger_server.models.user_model import UserRequest, User
from swagger_server.repository.user_repository import UserRepository

from pydantic import parse_obj_as
from typing import List

class UserUseCase():

    def __init__(self):
        self.user_repository = UserRepository()

    def create(self, request: UserRequest):
        # TO DO: crear validacion para ingresos de tareas
        response = self.user_repository.create(request.dict())
        user = parse_obj_as(User, response)
        return user    
    
    def create(self, user_id):
        response = self.user_repository.delete(user_id)
        user = parse_obj_as(User, response)
        return user
    
    def get_by_id(self, user_id):
        response = self.user_repository.get_by_id(user_id)
        user = parse_obj_as(User, response)
        return user
    
    def get_all(self):
        response = self.user_repository.get_all()
        users = parse_obj_as(List[User], response)
        return users
    
    def update(self, request, user_id):
        response = self.user_repository.update(request.dict(), user_id)
        user = parse_obj_as(User, response)
        return user