from swagger_server.models.task_model import TaskCreateRequest, Task
from swagger_server.repository.task_repository import TaskRepository

from swagger_server.resources.mail import EmailManager

from pydantic import parse_obj_as
from typing import List

class TaskUseCase():

    def __init__(self):
        self.task_repository = TaskRepository()
        self.email_manager = EmailManager()
        

    def create(self, request: TaskCreateRequest):
        # TO DO: crear validacion para ingresos de tareas
        response = self.task_repository.create(request.dict())

        # enviar correo se asignacion de tarea
        self.email_manager.send_email(
            message="Se te asigno una tarea",
            receiver_email="jupiterh@globalhitss.com",
            subject="Asignacion de Tarea"
        )

        task = parse_obj_as(Task, response)
        return task
    
    def get_all(self):
        response = self.task_repository.get_all()
        tasks = parse_obj_as(List[Task], response)
        return tasks
    
    def get_by_id(self, task_id):
        response = self.task_repository.get_by_id(task_id)
        task = None
        if response:
            task = parse_obj_as(Task, response[0])
        return task
    
    def delete(self, task_id):
        response = self.task_repository.delete(task_id)
        return response
    
    def update (self, request, task_id):
        response = self.task_repository.update(request.dict(), task_id)
        task = parse_obj_as(Task, response)
        return task
    
    def get_paginated(self, page, size, name):
        response = self.task_repository.get_paginated(page, size, name)
        tasks = parse_obj_as(List[Task], response)
        return tasks