from pydantic import BaseModel
from typing import Union, List

from swagger_server.models.task_model import Task
from swagger_server.models.user_model import User

class ErrorResponse(BaseModel):
    error_code: int
    error_message: str

class MessageResponse(BaseModel):
    message: str

class APIResponse(BaseModel):
    success: bool
    data: Union[
        None,
        Task,
        List[Task],
        ErrorResponse,
        MessageResponse,
        User
    ]