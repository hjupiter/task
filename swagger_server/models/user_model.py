from pydantic import BaseModel
from typing import Union

class User(BaseModel):
    id: int
    name: Union[str, None]
    email: Union[str, None]

class UserRequest(BaseModel):
    name: Union[str, None]
    email: Union[str, None]