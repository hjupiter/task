from pydantic import BaseModel, validator
from datetime import date
from typing import Union

from swagger_server.utils.generic_validators import GenericValidator

class Task(BaseModel):
    id: int
    name: Union[str, None]
    description: Union[str, None]
    user_id: Union[str, None]
    state: Union[str, None]
    start_date: date
    due_date: date

    @validator('start_date', 'due_date', pre=True)
    def validate_date(cls, value):
        return GenericValidator.validate_date_format(value)

class TaskCreateRequest(BaseModel):
    name: Union[str, None]
    description: Union[str, None]
    user_id: Union[str, None]
    state: Union[str, None]
    start_date: date
    due_date: date

    @validator('start_date', 'due_date', pre=True)
    def validate_date(cls, value):
        return GenericValidator.validate_date_format(value)

