import connexion
from flask.views import MethodView
from swagger_server.utils.logs.logging import log as logging
from swagger_server.resources.postgres_db import PostgresClient


from swagger_server.uses_cases.user_use_case import UserUseCase
from swagger_server.models.response_model import APIResponse, ErrorResponse
from swagger_server.models.user_model import UserRequest

from timeit import default_timer
from flask import jsonify

class UserView(MethodView):

    def __init__(self):
        log = logging()
        self.postgresClient = PostgresClient()
        self.log = log

    def user_register(self):  # noqa: E501
        """Registro de tarea

        Enpoint para el registro de tareas # noqa: E501

        :param body: 
        :type body: dict | bytes

        :rtype: TaskResponse
        """
        try:
            star_time = default_timer()
            self.log.info("Funcion: %r - Paquete: %r - Mensaje: Inicio de transacción", "register_task", __name__)
            response = None
            user_created = None

            if connexion.request.is_json:
                request = UserRequest(**connexion.request.get_json())
                user_use_case = UserUseCase()
                user_created = user_use_case.create(request=request)

            api_response = APIResponse(
                success=True,
                data=user_created
            )
            response = api_response.dict()
            end_time = default_timer()
            self.log.info("Funcion: %r - Paquete: %r - Mensaje: Fin de transacción, procesada en: %r milisegundos", "register_task", __name__, round(end_time - star_time)* 1000)
            return jsonify(response)
        except Exception as err:
            end_time = default_timer()
            error_response = APIResponse(
                success=False,
                data=ErrorResponse(
                    error_code=1,
                    error_message=str(err)
                )
            )
            error = error_response.dict()
            return jsonify(error)
