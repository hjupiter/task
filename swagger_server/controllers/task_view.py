import connexion

from swagger_server.utils.logs.logging import log as logging
from swagger_server.uses_cases.task_use_case import TaskUseCase

from swagger_server.models.response_model import APIResponse, ErrorResponse, MessageResponse
from swagger_server.models.task_model import TaskCreateRequest

from flask.views import MethodView
from timeit import default_timer

from flask import jsonify

class TaskView(MethodView):

    def __init__(self):
        log = logging()
        self.log = log

    def task_paginated(self, page, size, name):
        try:
            start_time = default_timer()
            self.log.info("Funcion: %r - Paquete: %r - Mensaje: Inicio de transacción", "task_get_all", __name__)
            task_use_case = TaskUseCase()
            tasks = task_use_case.get_paginated(page, size, name)
            api_response = APIResponse(success=True, data=tasks)
            response = api_response.dict()
            end_time = default_timer()
            self.log.info("Funcion: %r - Paquete: %r - Mensaje: Fin de transacción, procesada en: %r milisegundos", "task_get_all", __name__, round(end_time - start_time)* 1000)
            return jsonify(response)
        except Exception as err:
            end_time = default_timer()
            error_response = APIResponse(
                success=False,
                data=ErrorResponse(
                    error_code=1,
                    error_message=str(err)
                )
            )
            error = error_response.dict()
            return jsonify(error) 

    def task_get_all(self):
        try:
            start_time = default_timer()
            self.log.info("Funcion: %r - Paquete: %r - Mensaje: Inicio de transacción", "task_get_all", __name__)
            task_use_case = TaskUseCase()
            tasks = task_use_case.get_all()
            api_response = APIResponse(success=True, data=tasks)
            response = api_response.dict()
            end_time = default_timer()
            self.log.info("Funcion: %r - Paquete: %r - Mensaje: Fin de transacción, procesada en: %r milisegundos", "task_get_all", __name__, round(end_time - start_time)* 1000)
            return jsonify(response)
        except Exception as err:
            end_time = default_timer()
            error_response = APIResponse(
                success=False,
                data=ErrorResponse(
                    error_code=1,
                    error_message=str(err)
                )
            )
            error = error_response.dict()
            return jsonify(error) 
    
    def task_get_by_id(self, task_id):
        try:
            start_time = default_timer()
            self.log.info("Funcion: %r - Paquete: %r - Mensaje: Inicio de transacción", "task_get_by_id", __name__)
            task_use_case = TaskUseCase()
            task = task_use_case.get_by_id(task_id)
            api_response = None
            if task:
                api_response = APIResponse(success=True, data=task)
            else:
                api_response = APIResponse(success=True, data=MessageResponse(
                    message="No se encontro informacion"
                ))
            response = api_response.dict()
            end_time = default_timer()
            self.log.info("Funcion: %r - Paquete: %r - Mensaje: Fin de transacción, procesada en: %r milisegundos", "task_get_by_id", __name__, round(end_time - start_time)* 1000)
            return jsonify(response)
        except Exception as err:
            end_time = default_timer()
            error_response = APIResponse(
                success=False,
                data=ErrorResponse(
                    error_code=1,
                    error_message=str(err)
                )
            )
            error = error_response.dict()
            return jsonify(error) 
    
    def task_put_by_id(self, task_id):
        try:
            start_time = default_timer()
            self.log.info("Funcion: %r - Paquete: %r - Mensaje: Inicio de transacción", "task_put_by_id", __name__)

            task_updated = None

            if connexion.request.is_json:
                request = TaskCreateRequest(**connexion.request.get_json())
                task_use_case = TaskUseCase()
                task_updated = task_use_case.update(request=request, task_id=task_id)
                api_response = APIResponse(success=True, data=task_updated)
                response = api_response.dict()
                end_time = default_timer()
                self.log.info("Funcion: %r - Paquete: %r - Mensaje: Fin de transacción, procesada en: %r milisegundos", "task_put_by_id", __name__, round(end_time - start_time)* 1000)
                return jsonify(response)
        except Exception as err:
            end_time = default_timer()
            error_response = APIResponse(
                success=False,
                data=ErrorResponse(
                    error_code=1,
                    error_message=str(err)
                )
            )
            error = error_response.dict()
            return jsonify(error) 
    
    def task_delete_by_id(self, task_id):
        try:
            start_time = default_timer()
            self.log.info("Funcion: %r - Paquete: %r - Mensaje: Inicio de transacción", "task_delete_by_id", __name__)

            task_use_case = TaskUseCase()
            task_deleted = task_use_case.delete(task_id=task_id)

            if task_deleted:
                message_response = MessageResponse(
                    message="El registro ha sido eliminado"
                )
            else:
                message_response = ErrorResponse(
                    error_code=1,
                    error_message="El registro no logro ser eliminado"
                )
            
            api_response = APIResponse(success=True, data=message_response)
            response = api_response.dict()
            end_time = default_timer()
            self.log.info("Funcion: %r - Paquete: %r - Mensaje: Fin de transacción, procesada en: %r milisegundos", "task_delete_by_id", __name__, round(end_time - start_time)* 1000)
            return jsonify(response)
        except Exception as err:
            end_time = default_timer()
            error_response = APIResponse(
                success=False,
                data=ErrorResponse(
                    error_code=1,
                    error_message=str(err)
                )
            )
            error = error_response.dict()
            return jsonify(error) 

    def task_register(self):
        """Registro de tarea

        Enpoint para el registro de tareas # noqa: E501

        :param body: 
        :type body: dict | bytes

        :rtype: TaskResponse
        """
        try:
            star_time = default_timer()
            self.log.info("Funcion: %r - Paquete: %r - Mensaje: Inicio de transacción", "register_task", __name__)

            task_created = None

            if connexion.request.is_json:
                request = TaskCreateRequest(**connexion.request.get_json())
                task_use_case = TaskUseCase()

                task_created = task_use_case.create(request=request)
            api_response = APIResponse(
                success=True,
                data=task_created
            )
            response = api_response.dict()
            end_time = default_timer()
            self.log.info("Funcion: %r - Paquete: %r - Mensaje: Fin de transacción, procesada en: %r milisegundos", "register_task", __name__, round(end_time - star_time)* 1000)
            return jsonify(response)
        except Exception as err:
            end_time = default_timer()
            error_response = APIResponse(
                success=False,
                data=ErrorResponse(
                    error_code=1,
                    error_message=str(err)
                )
            )
            error = error_response.dict()
            return jsonify(error) 
