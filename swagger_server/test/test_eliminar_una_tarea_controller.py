# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.test import BaseTestCase


class TestEliminarUnaTareaController(BaseTestCase):
    """EliminarUnaTareaController integration test stubs"""

    def test_task_delete_by_id(self):
        """Test case for task_delete_by_id

        
        """
        response = self.client.open(
            '/HJUPITER/TASK/1.0.1/tasks/{taskId}'.format(task_id=56),
            method='DELETE')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
