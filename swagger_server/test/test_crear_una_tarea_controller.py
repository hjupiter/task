# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.task import Task  # noqa: E501
from swagger_server.test import BaseTestCase


class TestCrearUnaTareaController(BaseTestCase):
    """CrearUnaTareaController integration test stubs"""

    def test_task_register(self):
        """Test case for task_register

        Registro de tarea
        """
        body = Task()
        response = self.client.open(
            '/HJUPITER/TASK/1.0.1/tasks',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
