# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.user import User  # noqa: E501
from swagger_server.test import BaseTestCase


class TestRegistroDeUsuarioController(BaseTestCase):
    """RegistroDeUsuarioController integration test stubs"""

    def test_user_register(self):
        """Test case for user_register

        Registro de usuario
        """
        body = User()
        response = self.client.open(
            '/HJUPITER/TASK/1.0.1/user',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
