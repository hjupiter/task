from datetime import datetime

class GenericValidator:
    @classmethod
    def validate_date_format(cls, date_str):
        try:
            return datetime.strptime(date_str, '%d/%m/%Y').date()
        except ValueError:
            raise ValueError('Fecha en fomrato inválida')