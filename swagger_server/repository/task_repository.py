from swagger_server.models.task_model import Task
from swagger_server.resources.postgres_db import PostgresClient

class TaskRepository():

    def create(self, body: Task):
        with PostgresClient() as client:
            response = client.execute_insert('"TASK"',body)
            return response
        
    def get_all(self):
        with PostgresClient() as client:
            response = client.execute_query('SELECT * FROM "TASK"')
            return response
    
    def get_by_id(self, task_id):
        with PostgresClient() as client:
            response = client.execute_query('SELECT * FROM "TASK" WHERE ID=' + str(task_id))
            return response
        
    def delete(self, task_id):
        with PostgresClient() as client:
            response = client.execute_delete('"TASK"', 'id = ' + str(task_id))
            return response
        
    def update(self, body, task_id):
        with PostgresClient() as client:
            response = client.execute_update('"TASK"', body,'id = ' + str(task_id))
            return response
        
    def get_paginated(self, page, size, name):
        with PostgresClient() as client:
            table = '"TASK"'
            query = f"SELECT * FROM {table} WHERE name like '%{name}%'"
            response = client.execute_query_paginated(query, page, size)
            return response