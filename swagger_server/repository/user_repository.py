from swagger_server.resources.postgres_db import PostgresClient

class UserRepository():

    def create(self, body):
        with PostgresClient() as client:
            response = client.execute_insert(
                '"USER"', body
            )
            return response
        
    def get_all(self):
        with PostgresClient() as client:
            response = client.execute_query(
                'SELECT *  FROM "USER" '
            )
            return response
        
    def get_by_id(self, user_id):
        with PostgresClient() as client:
            response = client.execute_query(
                'SELECT *  FROM "USER" WHERE id = ' + str(user_id)
            )
            return response
        
    def update(self, body, user_id):
        with PostgresClient() as client:
            response = client.execute_update(
                '"USER"',
                body,
                'WHERE id = ' + str(user_id)
            )
            return response
        
    def delete(self, user_id):
        with PostgresClient() as client:
            response = client.execute_delete(
                '"USER"', "WHERE id = " + str(user_id)
            )
            return response