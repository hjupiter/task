from swagger_server.config.access import access
from swagger_server.utils.logs.logging import log as logging

import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

class EmailManager():

    def __init__(self):
        self.log = logging()

    def send_email(self, receiver_email, subject, message):
        credendiatls_db = EmailManager.get_credentials()
        sender_email = credendiatls_db["EMAIL"]
        sender_password = credendiatls_db["PASSWORD"]

        message = MIMEMultipart()
        message['From'] = sender_email
        message['To'] = receiver_email
        message['Subject'] = subject

        body = 'Este es un mensaje de Prueba'
        message.attach(MIMEText(body, 'plain'))

        try:
            smtp_server = 'smtp.mail.yahoo.com'
            smtp_port = 587
            server = smtplib.SMTP(smtp_server, smtp_port)
            server.ehlo()
            server.starttls()
            server.ehlo()

            server.login(sender_email, sender_password)

            server.sendmail(sender_email, receiver_email, message.as_string())

            server.quit()
        except Exception as err:
            self.log.critical(str(err))


    @staticmethod
    def get_credentials():
        response_json = access()
        credentials_db = response_json["EMAIL"]
        response = {
            "EMAIL": credentials_db["USER_EMAIL"],
            "PASSWORD": credentials_db["USER_PASSWORD"]
        }
        return response