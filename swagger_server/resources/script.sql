CREATE TABLE IF NOT EXISTS "USER" (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255),
    email VARCHAR(255)
);


CREATE TABLE IF NOT EXISTS "TASK" (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100),
    description VARCHAR(255),
    user_id INTEGER,
    state VARCHAR(1),
    start_date DATE,
    due_date DATE,
    FOREIGN KEY (user_id) REFERENCES "USER" (id)
);